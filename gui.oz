functor
import
    QTk at 'x-oz://system/wp/QTk.ozf'
    OS
    Application
    Open
export
    Create CreatePlayerWindow CreateWinnerWindow
    CaracterSelection
define
    Black = c(45 41 38)
    Orange = c(255 153 0)
    White = c(241 244 255)
    CD = {OS.getCWD}

    fun {NewActivePort Class Init}
        Obj = {New Class Init}
        P
    in
        thread S in
            {NewPort S P}
            for break:B Msg in S do 
                if Msg==kill then
                    {B}
                else
                    {Obj Msg} 
                end
            end
        end
        proc {$ Msg} {Send P Msg} end
    end

    class Gui
        attr grid
	         scorea scoreb scorec scored
	         playera playerb playerc playerd
             bomb bg boxd boxi
        meth init(h:H w:W handler:Handler <= proc {$ K} skip end pnames:PNames)
            Grid ScoreA ScoreB ScoreC ScoreD
            Window = {QTk.build td( bg:Black
                title: "BomberOz: Let it explode"
                grid(handle:Grid bg:white borderwidth:0)
                td( bg:Black
    				lr( label(text:PNames.a#": " bg:Black fg:White) 
                        label(text:"Game Over" handle:ScoreA bg:Black fg:White) glue:s )
                    lr( label(text:PNames.b#": " bg:Black fg:White) 
                        label(text:"Game Over" handle:ScoreB bg:Black fg:White) glue:s )
                    lr( label(text:PNames.c#": " bg:Black fg:White) 
                        label(text:"Game Over" handle:ScoreC bg:Black fg:White) glue:s )
                    lr( label(text:PNames.d#": " bg:Black fg:White) 
                        label(text:"Game Over" handle:ScoreD bg:Black fg:White) glue:s )
                )
                action:proc{$}{Application.exit 0} end % quit app gracefully on window closing
            )}
        in
            {Window bind(event:"<KeyPress>" args:[atom('A')] action:Handler)}

            %for I in 1..H-1 do
            %    {Grid configure(lrline column:1 columnspan:W+W-1 row:I*2 sticky:we)}
            %end
            %for I in 1..W-1 do
            %    {Grid configure(tdline row:1 rowspan:H+H-1 column:I*2 sticky:ns)}
            %end
            for I in 1..W do
                {Grid columnconfigure(I+I-1 minsize:40)} %43
            end
            for I in 1..H do
                {Grid rowconfigure(I+I-1 minsize:40)}
            end

            {Window show}

            grid := Grid
            scorea := ScoreA
            scoreb := ScoreB
            scorec := ScoreC
            scored := ScoreD
            bg := {QTk.newImage photo(file:CD#'/sprites/white.gif')}
            boxd := {QTk.newImage photo(file:CD#'/sprites/crate.gif')}
            boxi := {QTk.newImage photo(file:CD#'/sprites/cratesolid.gif')}
        end

        meth setskins(S)
            playera := S.a
            playerb := S.b
            playerc := S.c
            playerd := S.d
        end

        meth player(Name X Y) Img in
            case Name
            of 'a' then Img = @playera
            [] 'b' then Img = @playerb
            [] 'c' then Img = @playerc
            [] 'd' then Img = @playerd
            else skip
            end
            {@grid configure(label(image:Img borderwidth:0) row:X+X-1 column:Y+Y-1)}
        end

        meth score(Name X) S in
            case Name
            of 'a' then S = @scorea
            [] 'b' then S = @scoreb
            [] 'c' then S = @scorec
            [] 'd' then S = @scored
            else skip
            end
            {S set(""#X)}
        end

        meth explosion(X Y Axe)
            {@grid configure(label(
                        image:{QTk.newImage photo(file:CD#'/sprites/fire'#Axe#'3.gif')}
                        borderwidth:0) row:X+X-1 column:Y+Y-1)}
        end

        meth bomb(Pos Level TimeLeft)
            fun {Append A B}
                case A
                of H|T then
                    H|{Append T B}
                [] nil then
                    B
                end
            end
        in
            case Pos
            of pos(X Y) then
                if TimeLeft == 0 then
                    {@grid configure(label(
                        image:{QTk.newImage photo(file:CD#'/sprites/fireCenter1.gif')}
                        borderwidth:0) row:X+X-1 column:Y+Y-1)}
                else
                    {@grid configure(label(
                        image:{QTk.newImage photo(file:CD#'/sprites/bombv2_'#{Append {Int.toString Level} {Int.toString TimeLeft}}#'.gif')}
                        borderwidth:0) row:X+X-1 column:Y+Y-1)}
                end
            else
                skip
            end
        end

        meth reset(X Y)
            {@grid configure(label(image:@bg borderwidth:0) row:X+X-1 column:Y+Y-1)}
        end
        
        meth box(X Y T) Img in
            case T
            of 'boxd' then Img = @boxd
            [] 'boxi' then Img = @boxi
            else skip
            end
            {@grid configure(label(image:Img borderwidth:0) row:X+X-1 column:Y+Y-1)}
        end

        meth food(X Y)
            {@grid configure(label(image:{QTk.newImage photo(file:CD#'/sprites/food/'#{OS.rand} mod 7 + 1#'.gif')} borderwidth:0) row:X+X-1 column:Y+Y-1)}
        end
    end

    %% create the GUI object
    fun {Create Init}
        {NewActivePort Gui Init}
    end

    %% create win window
    proc {CreateWinnerWindow Winner}
        {{QTk.build td( bg:Black
            title: "BomberOz: Game Over"
            lr( bg:Black
            label(init:Winner#" won the game!" width:50 height:20 bg:Black fg:White)
            td( bg:Black
                button(text:"Exit" width:10 height:5 action:proc{$} {Application.exit 0} end bg:Orange fg:Black activebackground:Black activeforeground:Orange))
            )
        action:proc{$} {Application.exit 0} end)} show}
    end
    
    %% create window where user can modify settings
    proc {CreatePlayerWindow ?MaxP ?NamesP ?Bonus ?CanGoOpposite ?LevelN}
        Red=c(255 0 0) Green=c(0 255 0)
        NbrPlayers Name1 Name2 Name3 Name4 BonusPassword
        HP1 HP2 HP3 HP4 HCanGoOpposite
        CurrentMap ChooseMap CSavedMap = {NewCell "dedale"}
        CP1 = {NewCell "Player 1"} CP2 = {NewCell "Player 2"} CP3 = {NewCell "Player 3"} CP4 = {NewCell "Player 4"} CBP = {NewCell "Password for bonus"}

        proc {WriteNamesInFile Text}
            try 
                File={New Open.file init(name:"settings.dat" flags:[write create truncate])}
            in 
                {File write(vs:Text)}
                {File close}
            catch _ then skip end 
        end 

        proc {LoadNamesInFile}
            fun {Scan Text Word NbrChar NbrWord}
                if Text == nil andthen NbrWord == 0 then ["Player 1" "Player 2" "Player 3" "Player 4" "dedale"]
                else
                    if NbrWord >= 5 then nil
                    else
                        case Text
                        of nil then Word|nil
                        [] H|T then
                            if H == 10 then Word|{Scan T nil 0 NbrWord+1}
                            elseif H == 9 orelse NbrChar >= 15 then {Scan T Word NbrChar NbrWord}
                            else {Scan T {Append Word {Atom.toString {Char.toAtom H}}} NbrChar+1 NbrWord} end
                        else nil end
                    end
                end
            end

            fun {GetNElem L N}
                case L of nil then nil
                [] H|T then if N == 1 then H else {GetNElem T N-1} end
                end
            end
        in
            try 
                File = {New Open.file init(name:"settings.dat")}
                Contents = {File read(list:$ size:all)}
                Words = {Scan Contents "" 0 0}
            in
                CP1 := {GetNElem Words 1} {HP1 set(@CP1)}
                CP2 := {GetNElem Words 2} {HP2 set(@CP2)}
                CP3 := {GetNElem Words 3} {HP3 set(@CP3)}
                CP4 := {GetNElem Words 4} {HP4 set(@CP4)}
                CSavedMap := {GetNElem Words 5} {CurrentMap set(@CSavedMap)}
                {File close}
            catch _ then {WriteNamesInFile "Player 1\nPlayer 2\nPlayer 3\nPlayer 4\ndedale"} end 
        end

        proc {WriterAnalyser Key Val}
            MaxChar H C
            fun {Looper L A}
                case L of nil then A
                [] _|T then {Looper T A+1} end
            end
        in
            if Val == 1 then C = CP1 H = HP1
            elseif Val == 2 then C = CP2 H = HP2
            elseif Val == 3 then C = CP3 H = HP3
            elseif Val == 4 then C = CP4 H = HP4
            elseif Val == 5 then C = CBP H = BonusPassword
            else skip end

            case Key of '\t' then {H set(@C)}
            [] '\r' then 
                {H set(@C)}
                if Val == 5 then {BonusButton} end
            else
                MaxChar = {Looper {H getText(p(1 0) 'end' $)} 0}
                if MaxChar >= 22 then {H set(@C)}
                elseif Val \= 5 andthen MaxChar >= 17 then {H set(@C)} 
                else C:={H getText(p(1 0) p(1 MaxChar) $)} end
            end
        end

        fun {BonusActivated}
            if {String.toAtom {BonusPassword getText(p(1 0) 'end' $)}} == 'ln0=0\n' then true
            else false end
        end
        proc {BonusButton} 
            if {BonusActivated} then {BonusPassword set(bg:Green)}
            else {BonusPassword set(bg:Red)} end
            thread {Delay 5000} try {BonusPassword set(bg:Orange)} 
                catch X then /*Check if the error is waited else raise */
                    case X of
                        error(1:E debug:_) then
                        case E of qtk(_ _ configure(bg:c(v([50 53 53]) v([49 53 51]) v([48])))) then /*Expected bug*/
                            skip
                        else {Exception.'raise' X}
                        end
                    else {Exception.'raise' X}
                    end
                end 
            end
        end

        %proc{PlayerNbr1} NbrPlayers = 1 Bonus={BonusActivated} {WindowPlayer close} end
        proc{PlayerNbr2} NbrPlayers = 2 Bonus={BonusActivated} {WindowPlayer close} end
        proc{PlayerNbr3} NbrPlayers = 3 Bonus={BonusActivated} {WindowPlayer close} end
        proc{PlayerNbr4} NbrPlayers = 4 Bonus={BonusActivated} {WindowPlayer close} end
        

        WindowPlayer = {QTk.build td( bg:Black
            title: "BomberOz: Settings"
            td( bg:Black 
                lr( label(init:"Choose the name of each players:" bg:Black fg:White))
                lr( text(init:@CP1 width:15 height:1 wrap:char takefocus:true return:Name1 handle:HP1
                        bg:Orange foreground:Black insertbackground:Black selectbackground:Black selectforeground:Orange)
                    text(init:@CP2 width:15 height:1 wrap:char takefocus:true return:Name2 handle:HP2
                        bg:Orange foreground:Black insertbackground:Black selectbackground:Black selectforeground:Orange)
                    text(init:@CP3 width:15 height:1 wrap:char takefocus:true return:Name3 handle:HP3
                        bg:Orange foreground:Black insertbackground:Black selectbackground:Black selectforeground:Orange)
                    text(init:@CP4 width:15 height:1 wrap:char takefocus:true return:Name4 handle:HP4
                        bg:Orange foreground:Black insertbackground:Black selectbackground:Black selectforeground:Orange)
                )
            )
            td( bg:Orange
                lr( label(init:"Choose your map:"bg:Black fg:White))
                lr( 
                    label(init:@CSavedMap handle:CurrentMap return:LevelN bg:Orange fg:Black)
                    dropdownlistbox(init:[dedale wood snow oz classical random] handle:ChooseMap
                        background:Black buttonbackground:Orange
                        highlightbackground:Black highlightcolor:Black
                        selectbackground:Orange selectforeground:Black
                        buttonhighlightbackground:Black buttonhighlightcolor:Orange
                        buttonactivebackground:Orange buttonactiveforeground:Orange
                        action:proc{$} CSavedMap:={List.nth {ChooseMap get($)} {ChooseMap get(firstselection:$)}} {CurrentMap set(@CSavedMap)} end
                    )
                )
            )
            td( bg:Black
                lr( label(init:"Choose the number of players:" bg:Black fg:White))
                lr(
                    %button(text:"1\n Player"  action:PlayerNbr1 cursor:hand1 width:15 height:20 glue:we 
                    %    bg:Orange fg:Black activebackground:Black activeforeground:Orange)
                    button(text:"2\n Players" action:PlayerNbr2 cursor:hand1 width:15 height:20 glue:we 
                        bg:Orange fg:Black activebackground:Black activeforeground:Orange)
                    button(text:"3\n Players" action:PlayerNbr3 cursor:hand1 width:15 height:20 glue:we 
                        bg:Orange fg:Black activebackground:Black activeforeground:Orange)
                    button(text:"4\n Players" action:PlayerNbr4 cursor:hand1 width:15 height:20 glue:we 
                        bg:Orange fg:Black activebackground:Black activeforeground:Orange)
                )
            )
            td(
                bg:Black
                lr(
                    text(init:@CBP width:20 height:1 handle:BonusPassword 
                        bg:Orange foreground:Black insertbackground:Black selectbackground:Black selectforeground:Orange)
                    button(text:">" action:BonusButton width:2 height:1 glue:we 
                        bg:Orange fg:Black activebackground:Black activeforeground:Orange
                    )
                )
            )
            td( bg:Black
                lr( label(init:"Options:" bg:Black fg:White))
                checkbutton( bg:Green fg:Black activebackground:Green activeforeground:Black
                    text:"Players can move in the opposite way"
                    init:true
                    handle:HCanGoOpposite
                    return:CanGoOpposite
                    action:proc{$}
                        if {HCanGoOpposite get($)}==false then
                            {HCanGoOpposite set(false text:"Players cannot move in the opposite way" bg:Red activebackground:Red)}
                        else {HCanGoOpposite set(true text:"Players can move in the opposite way" bg:Green activebackground:Green)} end
                    end
                )
            )
            action:proc{$} {WriteNamesInFile @CP1#"\n"#@CP2#"\n"#@CP3#"\n"#@CP4#"\n"#@CSavedMap} {Application.exit 0} end % quit app gracefully on window closing
        )}
    in
        {LoadNamesInFile}
        {WindowPlayer show}
        {HP1 bind(event:"<KeyPress>" args:[atom('A') int(1)] action:WriterAnalyser)}
        {HP2 bind(event:"<KeyPress>" args:[atom('A') int(2)] action:WriterAnalyser)}
        {HP3 bind(event:"<KeyPress>" args:[atom('A') int(3)] action:WriterAnalyser)}
        {HP4 bind(event:"<KeyPress>" args:[atom('A') int(4)] action:WriterAnalyser)}
        {BonusPassword bind(event:"<KeyPress>" args:[atom('A') int(5)] action:WriterAnalyser)}
        MaxP = NbrPlayers
        NamesP = pnames(a:Name1 b:Name2 c:Name3 d:Name4)
        {WriteNamesInFile NamesP.a#"\n"#NamesP.b#"\n"#NamesP.c#"\n"#NamesP.d#"\n"#LevelN}
    end

    proc {CaracterSelection NbrPlayers pnames(a:Name1 b:Name2 c:Name3 d:Name4) Path ?R}
        Skins={Array.new 1 4 none}
        C={NewCell 0}
        SP1 SP2 SP3 SP4
        Display = display(SP1 SP2 SP3 SP4)
        Anonymous={QTk.newImage photo(file:CD#'/sprites/'#Path#'/anonymous.gif')}
        WindowPlayer = {QTk.build td(
            bg:Black
            title:'Choose a player'
            td( td( bg:Black
                    lr( button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n1.gif')} action:proc {$} {Chose 1} end)
                        button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n2.gif')} action:proc {$} {Chose 2} end)
                        button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n3.gif')} action:proc {$} {Chose 3} end)
                        button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n4.gif')} action:proc {$} {Chose 4} end)
                )   )
                td( bg:Black
                    lr( button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n5.gif')} action:proc {$} {Chose 5} end)
                        button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n6.gif')} action:proc {$} {Chose 6} end)
                        button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n7.gif')} action:proc {$} {Chose 7} end)
                        button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n8.gif')} action:proc {$} {Chose 8} end)
                )   )
                td( bg:Black
                    lr( button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n9.gif')} action:proc {$} {Chose 9} end)
                        button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n10.gif')} action:proc {$} {Chose 10} end)
                        button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n11.gif')} action:proc {$} {Chose 11} end)
                        button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n12.gif')} action:proc {$} {Chose 12} end)
                )   )
                td( bg:Black
                    lr( button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n13.gif')} action:proc {$} {Chose 13} end)
                        button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n14.gif')} action:proc {$} {Chose 14} end)
                        button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n15.gif')} action:proc {$} {Chose 15} end)
                        button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n16.gif')} action:proc {$} {Chose 16} end)
                )   )
            )
            td( bg:Black
                lr( label(init:Name1 bg:Black fg:White width:16)
                    label(init:Name2 bg:Black fg:White width:16)
                    label(init:Name3 bg:Black fg:White width:16)
                    label(init:Name4 bg:Black fg:White width:16)
                )
                lr( button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/back.gif')} action:Back)
                    button(handle:SP1 image:Anonymous)
                    button(handle:SP2 image:Anonymous)
                    button(handle:SP3 image:Anonymous)
                    button(handle:SP4 image:Anonymous)
                    button(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/check.gif')} action:Validate)
            )   )
            action:proc{$} {Application.exit 0} end % quit app gracefully on window closing
        )}
        
        fun {IsNotInList N}
            fun {Aux N C}
                if C > NbrPlayers then true
                else
                    if {Array.get Skins C} == N then false
                    else {Aux N C+1}
                    end
                end
            end
        in
            {Aux N 1}
        end

        proc {Chose N}
            if @C < NbrPlayers then
                if {IsNotInList N} == true then
                    C := @C + 1
                    {Array.put Skins @C N} % 
                    {Display.@C set(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/n'#N#'.gif')})}
                else skip
                end
            else
                skip
            end
        end
        proc {Back}
            if @C > 0 then
                {Display.@C set(image:Anonymous)}
                {Array.put Skins @C none}
                C := @C - 1
            else skip
            end
        end
        proc {Validate}
            fun {GetSkin I}
                if {Array.get Skins I} == none then none
                else
                    {QTk.newImage photo(file:CD#'/sprites/'#Path#'/'#{Array.get Skins I}#'.gif')}
                end
            end
        in
            if @C == NbrPlayers then
                R=skins(
                    a:{GetSkin 1}
                    b:{GetSkin 2}
                    c:{GetSkin 3}
                    d:{GetSkin 4}
                    )
                {WindowPlayer close}
            else skip
            end
        end
    in
        {WindowPlayer show}
        for I in NbrPlayers+1..4 do
            {Display.I set(image:{QTk.newImage photo(file:CD#'/sprites/'#Path#'/anonymous2.gif')})}
        end
    end
end
