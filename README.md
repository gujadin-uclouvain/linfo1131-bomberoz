## **LINFO1131 : Projet** => **BomberOZ**
---

## **MEMBERS:**
- Samy Bettaieb
- Félix Gaudin
- Guillaume Jadin

## **MAKEFILE:**
`$ make all` -> compile le programme

`$ make run` -> compile et lance le programme
> On affiche des informations dans le terminal penant que le programme charge les données

`$ make clean` -> supprime les fichiers compilés (.oza/.ozf) et les fichiers de données (.dat)

## **RÈGLES DU JEU:**
- Les joueurs doivent rester en vie, le dernier a gagné.
- Pour tuer un joueur, il suffit de poser une bombe et que le souffle de celle-ci touche le joueur.
- Les joueurs peuvent ramasser de la nourriture pour augmenter la puissance de leurs bombes ainsi que leurs nombres.

## **COMMANDES:**
(Respectivement : **haut -- bas -- gauche -- droite  ::  placer_une_bombe**)

Player 1:

- **z -- s -- q -- d  ::  e**

Player 2:

- **8 -- 5 -- 4 -- 6  ::  9**  (au pavé numérique)

Player 3:

- **t -- g -- f -- h  ::  y**

Player 4:

- **o -- l -- k -- m  ::  p**


*En fonction de l'option pour activer ou désactiver les mouvements opposée (dans les fenêtre des réglages) les joueurs pourront aller dans le sens inverse de leur dernier mouvement ou non.*

## **SCORES:**
- Les scores évoluent en ramassant de la nourriture.
- Le minimum est **11** et le maximum est **33**.
- Le premier nombre représente le nombre de bombes pouvant être posés et le second correspond à la puissance des bombes:

> Par exemple, un joueur avec un score de **23** pourra poser, au maximum, **2 bombes de puissance 3**.

- Les scores ne diminuent pas.


## **BITBUCKET:**
Le lien de notre [BitBucket](https://bitbucket.org/gujadin/project-linfo1131-bomberoz/src/master/).

> Si vous rencontrez un soucis, veuillez nous le dire par mail:
> [Samy](mailto:samy.bettaieb@student.uclouvain.be),
> [Félix](mailto:felix.gaudin@student.uclouvain.be),
> [Guillaume](mailto:guillaume.l.jadin@student.uclouvain.be)

