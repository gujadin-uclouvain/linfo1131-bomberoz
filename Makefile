
SRCS:=$(wildcard *.oz)
OZFS:=$(SRCS:=f) # Add an 'f' to turn .oz into .ozf

MAKEFLAGS+=--no-builtin-rules

.SUFFIXES:
.SUFFIXES: .oz .ozf

%.ozf : %.oz
	ozc -c $< -o $@

.PHONY: all
all: $(OZFS)

.PHONY: run
run: main.ozf all
	ozengine $<

clean:
	-rm *.ozf *.dat
