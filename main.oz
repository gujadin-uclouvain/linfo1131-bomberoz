functor
import
    Application
    GUI at './gui.ozf'
    Property
    System  % provides System.{show,showInfo,print,printInfo}
    OS
define
    %-------------------------------------------------------------------------------------------------------
    % Active Port Object Creator
    %-------------------------------------------------------------------------------------------------------
    fun {NewActivePort Class Init}
        Obj = {New Class Init}
        P
    in
        thread S in
            {NewPort S P}
            for break:B Msg in S do 
                if Msg==kill then
                    {B}
                else
                    {Obj Msg} 
                end
            end
        end
        proc {$ Msg} {Send P Msg} end
    end

    %-------------------------------------------------------------------------------------------------------
    % Key Press Events - Handler
    %-------------------------------------------------------------------------------------------------------
    proc {Handler Key}
        case Key
        % Movements P1
        of 'z' then if {IsNotDead 1} then {Game canmove(port:P1 axe:x direction:~1)} else skip end
        [] 's' then if {IsNotDead 1} then {Game canmove(port:P1 axe:x direction: 1)} else skip end
        [] 'q' then if {IsNotDead 1} then {Game canmove(port:P1 axe:y direction:~1)} else skip end
        [] 'd' then if {IsNotDead 1} then {Game canmove(port:P1 axe:y direction: 1)} else skip end
        % Movements P2
        [] '8' then if {IsNotDead 2} then {Game canmove(port:P2 axe:x direction:~1)} else skip end
        [] '5' then if {IsNotDead 2} then {Game canmove(port:P2 axe:x direction: 1)} else skip end
        [] '4' then if {IsNotDead 2} then {Game canmove(port:P2 axe:y direction:~1)} else skip end
        [] '6' then if {IsNotDead 2} then {Game canmove(port:P2 axe:y direction: 1)} else skip end
        % Movements P3
        [] 't' then if {IsNotDead 3} then {Game canmove(port:P3 axe:x direction:~1)} else skip end
        [] 'g' then if {IsNotDead 3} then {Game canmove(port:P3 axe:x direction: 1)} else skip end
        [] 'f' then if {IsNotDead 3} then {Game canmove(port:P3 axe:y direction:~1)} else skip end
        [] 'h' then if {IsNotDead 3} then {Game canmove(port:P3 axe:y direction: 1)} else skip end
        % Movements P4
        [] 'o' then if {IsNotDead 4} then {Game canmove(port:P4 axe:x direction:~1)} else skip end
        [] 'l' then if {IsNotDead 4} then {Game canmove(port:P4 axe:x direction: 1)} else skip end
        [] 'k' then if {IsNotDead 4} then {Game canmove(port:P4 axe:y direction:~1)} else skip end
        [] 'm' then if {IsNotDead 4} then {Game canmove(port:P4 axe:y direction: 1)} else skip end
        % Set a Bomb
        [] 'e' then if {IsNotDead 1} then {P1 setbomb()} else skip end
        [] '9' then if {IsNotDead 2} then {P2 setbomb()} else skip end
        [] 'y' then if {IsNotDead 3} then {P3 setbomb()} else skip end
        [] 'p' then if {IsNotDead 4} then {P4 setbomb()} else skip end
        else skip
        end
    end

    %-------------------------------------------------------------------------------------------------------
    % Helpers
    %-------------------------------------------------------------------------------------------------------
    proc {NextPosition X Y Axe Direction ?NewX ?NewY}
        case Axe
        of x then
            NewX = X + Direction
            NewY = Y
        [] y then
            NewX = X
            NewY = Y + Direction
        else
            {System.show error}
        end
    end

    fun {FindIndexe X Y} ((X-1)*WIDTH)+Y end

    fun {IsObstacle Map}
        fun {IsObstacleRec L}
            case L of nil then false
            [] H|T then
                case H
                of boxi then true
                [] boxd then true
                [] bomb then true
                else {IsObstacleRec T} end
            end
        end
    in
        {IsObstacleRec Map}
    end

    fun {IsNotDead Nbr} PlayerIsAlive in
        {Game playerisalive(player:{String.toAtom [96+Nbr]} result:PlayerIsAlive)}
        if NbrPlayers >= Nbr andthen PlayerIsAlive == true then true
        else false
        end
    end

    %-------------------------------------------------------------------------------------------------------
    % Active Objects
    %-------------------------------------------------------------------------------------------------------
    class Server
        attr map players
        meth init(lo:L hi:H players:P)
            fun {CreateListPlayer Nbr Acc}
                if Nbr == Acc then nil
                else
                    {String.toAtom [97+Acc]}|{CreateListPlayer Nbr Acc+1}
                end
            end
        in
            map := {NewArray L H nil}
            players := {CreateListPlayer P 0}
        end

        meth playerisalive(player:P result:?R)
            fun {IsAlive L P}
                case L of nil then false
                [] H|T then
                    if P == H then true
                    else {IsAlive T P} end
                end
            end
        in
            R = {IsAlive @players P}
        end

        meth canmove(port:P axe:Axe direction:D) X Y DP in
            DP = {P getlocation($)} % dataplayer
            {NextPosition DP.x DP.y Axe D X Y}
            if {IsObstacle @map.{FindIndexe X Y}} == false then
                {self playermove(
                    port:P
                    current:DP
                    next:move(x:X y:Y opm:move(axe:Axe direction:~D) axe:Axe direction:D)
                )}
            else
                Nx Ny in
                {NextPosition DP.x DP.y Axe ~D Nx Ny} % Show opposite direction
                if {IsObstacle @map.{FindIndexe Nx Ny}} == false then 
                    {self playermove(
                        port:P
                        current:DP
                        next:move(x:Nx y:Ny opm:move(axe:Axe direction:D) axe:Axe direction:D)
                    )}
                else skip end % Not moving
            end
        end

        meth playermove(port:P current:DP next:NextMove) NextIndex Index in
            if DP.opm \= move(axe:NextMove.axe direction:NextMove.direction) then
                Index = {FindIndexe DP.x DP.y}
                NextIndex = {FindIndexe NextMove.x NextMove.y}

                % Remove player in current place
                case @map.Index 
                of bomb|_ then skip
                else{Gui reset(DP.x DP.y)} end
                {self remove(index:Index value:DP.id where:map)}
                
                % Display element just behind
                case @map.Index of nil then skip
                [] H|_ then
                    case H
                    of 'a' then {Gui player(a DP.x DP.y)}
                    [] 'b' then {Gui player(b DP.x DP.y)}
                    [] 'c' then {Gui player(c DP.x DP.y)}
                    [] 'd' then {Gui player(d DP.x DP.y)}
                    else skip end
                end

                % Display player in next place
                case @map.NextIndex
                of explosion|_ then {self killplayer(port:P index:NextIndex idplayer:DP.id)}
                else
                    case @map.NextIndex of food|_ then 
                        {P incscore()} 
                        {self remove(index:NextIndex value:'food' where:map)}
                        {Gui reset(NextMove.x NextMove.y)}
                    else skip
                    end
                    {self add(index:NextIndex value:DP.id)}
                    {P move(x:NextMove.x y:NextMove.y opm:NextMove.opm)}
                end
            else /*{System.show 'Opposite movement'}*/skip end
        end

        meth bombcanmove(X Y Resp) Index in
            Index = {FindIndexe X Y}
            Resp = {IsObstacle @map.Index}
            if Resp == true then
                case @map.Index
                of boxd|_ then
                    {BoxDestroyer destroy(x:X y:Y type:'boxd')}
                else skip end
            else
                {self killatcase(index:Index)}
            end
        end

        meth killatcase(index:Index)
            for Id in @map.Index do
                case Id
                    of 'a' then {self killplayer(port:P1 index:Index idplayer:Id)}
                    [] 'b' then {self killplayer(port:P2 index:Index idplayer:Id)}
                    [] 'c' then {self killplayer(port:P3 index:Index idplayer:Id)}
                    [] 'd' then {self killplayer(port:P4 index:Index idplayer:Id)}
                    [] 'food' then {self remove(index:Index value:'food' where:map)}
                    else skip
                end
            end
        end

        meth playerwin(result:?R)
            case @players of nil then R = nil
            [] H|T then
                if T == nil then R = H else R = false end
            else R = false end
        end

        meth killplayer(port:P index:I idplayer:Id) IsWinner in
            {P setscore(displayscore:"Game Over")}
            {P kill}
            {self remove(index:~1 value:Id where:players)}
            {self remove(index:I value:Id where:map)}
            {self playerwin(result:IsWinner)}
            case IsWinner of false then skip
            [] nil then {Delay 4000} {Application.exit 0}
            else {GUI.createWinnerWindow PlayerNames.IsWinner} end
        end

        meth add(index:I value:V)
            if V == 'boxi' orelse V == 'boxd' then
                case @map.I
                of H|_ then
                    if H == V then skip
                    else @map.I := V|@map.I end
                else @map.I := V|@map.I end
            else @map.I := V|@map.I end
        end

        meth remove(index:I value:E where:Attr)
            fun {RemoveElem E L A}
                case L
                of nil then {Reverse A}
                [] H|T then 
                    if H == E then {RemoveElem E T A}
                    else {RemoveElem E T H|A}
                    end
                end
            end
        in
            case Attr
            of map then @map.I := {RemoveElem E @map.I nil}
            [] players then players := {RemoveElem E @players nil}
            end
        end

        meth emptyindex(index:Index empty:Empty)
            case @map.Index 
            of nil then Empty = true
            else Empty = false
            end
        end
    end

    class Player
        attr x y id score opm bombSet
        meth init(x:X y:Y id:Id)
            x:=X
            y:=Y
            id:=Id
            {self setscore(displayscore:"1-1")}
            opm:=nil
            bombSet := 1
            {Game add(index:((X-1)*WIDTH)+Y value:Id)}
            {Gui player(Id X Y)}
        end

        meth getlocation($)
            location(x:@x y:@y opm:@opm id:@id)
        end
        
        meth move(x:X y:Y opm:Opm)
            x:=X
            y:=Y
            if {Not CanGoOpposite} then opm:=Opm else skip end
            {Gui player(@id X Y)}
        end

        meth getPower(Power Resp)
            case Power
            of 0 then Resp = 3
            [] 1 then Resp = 5
            [] 2 then Resp = 9
            end
        end

        meth setbomb() CX=@x CY=@y
            CPower=@score mod 3
            proc {WaitUntilBOOM Level TimeLeft}
                {Gui bomb(pos(CX CY) Level TimeLeft)}
                if TimeLeft == 0 then ExplosionPorts I1 I2 I3 I4 in
                    ExplosionPorts = explosions(
                        {NewActivePort Explosion init(x:CX y:CY axe:y direction:~1 power:{self getPower(CPower $)} isDead:I1)} /*Up*/
                        {NewActivePort Explosion init(x:CX y:CY axe:y direction:1  power:{self getPower(CPower $)} isDead:I2)} /*Down*/
                        {NewActivePort Explosion init(x:CX y:CY axe:x direction:1  power:{self getPower(CPower $)} isDead:I3)} /*Right*/
                        {NewActivePort Explosion init(x:CX y:CY axe:x direction:~1 power:{self getPower(CPower $)} isDead:I4)} /*Down*/
                    ) % BOOM
                    {Wait I1}
                    {Wait I2}
                    {Wait I3}
                    {Wait I4}
                    thread 
                        {ExplosionPorts.1 kill}
                        {ExplosionPorts.2 kill}
                        {ExplosionPorts.3 kill}
                        {ExplosionPorts.4 kill}
                    end
                else
                    {Delay 1000}
                    {WaitUntilBOOM CPower+1 TimeLeft-1}
                end
            end
        in
            if @bombSet \= 0 then
                bombSet := @bombSet-1
                {Game add(index:{FindIndexe CX CY} value:'bomb')}            
                thread
                    {WaitUntilBOOM CPower+1 4}
                    {Game killatcase(index:{FindIndexe CX CY})}
                    {Delay 1000}
                    {Gui reset(CX CY)}
                    {Game remove(index:{FindIndexe CX CY} value:'bomb' where:map)}
                    bombSet := @bombSet+1
                end
            else skip end
        end

        meth incscore()
            if @score < 8 then
                score := @score+1
                if(@score mod 3 == 0) then
                    bombSet := @bombSet+1
                else skip
                end
                {Gui score( @id (@score div 3)+1#"-"#((@score mod 3)+1) )}
            end
        end

        meth setscore(displayscore:S)
            score:=0
            {Gui score(@id S)}
        end
    end

    class Explosion
        attr power axe direction x y dead
        meth init(x:X y:Y axe:Axe direction:Direction power:Power isDead:Dead)
            power := Power 
            axe := Axe 
            direction := Direction 
            x := X 
            y := Y
            dead := Dead
            {self canmove()}
        end

        meth canmove()
            NewX NewY
        in
            if @power == 0 then
                @dead=unit
            else
                {NextPosition @x @y @axe @direction NewX NewY}
                if {Game bombcanmove(NewX NewY $)} == false then
                    x:=NewX
                    y:=NewY
                    power:=@power-1
                    {self display()}
                    {Game add(index:{FindIndexe NewX NewY} value:explosion)}

                    thread
                        {Delay 1000}
                        {Gui reset(NewX NewY)}
                        {Game remove(index:{FindIndexe NewX NewY} value:explosion where:map)}

                    end
                    {self canmove()}
                else
                    @dead=unit
                end
            end
        end

        meth display()
            {Gui explosion(@x @y @axe)}
        end

        meth isDead($)
            @dead
        end
    end

    %-------------------------------------------------------------------------------------------------------
    % Box Generator
    %-------------------------------------------------------------------------------------------------------
    proc {BoxCreator Msg}
        case Msg 
        of set(x:X y:Y type:T) then Index in
            Index = {FindIndexe X Y}
            {Game add(index:Index value:T)}
            {Gui box(X Y T)}

        [] setWall() then
            for I in 1..HEIGHT do
                {BoxCreator set(x:I y:1 type:'boxi')}
                {BoxCreator set(x:I y:WIDTH type:'boxi')}
            end
            for I in 2..WIDTH-1 do
                {BoxCreator set(x:1 y:I type:'boxi')}
                {BoxCreator set(x:HEIGHT y:I type:'boxi')}
            end

        [] setAllSpace() then
            for I in 2..WIDTH-1 do
                {BoxCreator setLine(axe:y index:I type:'boxd')}
            end

        [] setLine(axe:A index:L type:T) then
            case A of x then
                for I in 2..WIDTH-1 do
                    {BoxCreator set(x:L y:I type:T)}
                end
            [] y then
                for I in 2..HEIGHT-1 do
                    {BoxCreator set(x:I y:L type:T)}
                end
            else skip end

        [] setLinePoint(axe:A index:L type:T) then
            case A of x then
                for I in 2..WIDTH-1 do
                    if I mod 2 == 1 then
                    {BoxCreator set(x:L y:I type:T)}
                    else skip
                    end
                end
            [] y then
                for I in 2..HEIGHT-1 do
                    if I mod 2 == 1 then
                    {BoxCreator set(x:I y:L type:T)}
                    else skip
                    end
                end
            else skip end

        [] setSequentialLines(type:T) then
            local
                proc{Looper X Y IsPair}
                    if Y >= WIDTH then skip
                    elseif X >= HEIGHT then
                        if IsPair == true then {Looper 3 Y+2 false} 
                        else {Looper 2 Y+2 true} end
                    else
                        {BoxCreator set(x:X y:Y type:T)}
                        {Looper X+2 Y IsPair}
                    end
                end
            in
                {Looper 2 2 true}
            end

        else skip end
    end

    proc {BoxDestroyer Msg}
        case Msg 
        of destroy(x:X y:Y type:T) then Index in
            Index = {FindIndexe X Y}
            {Game remove(index:Index value:T where:map)}
            {Gui reset(X Y)}

        [] destroyTriangle(nbrplayers:NbrP type:T) then
            if NbrP >= 1 then 
                for I in 2..3 do 
                    for J in 2..3+2-I do 
                        {BoxDestroyer destroy(x:I y:J type:T)}
                    end
                end
                if NbrP >= 2 then
                    {BoxDestroyer destroy(x:HEIGHT-1 y:WIDTH-1 type:T)}
                    {BoxDestroyer destroy(x:HEIGHT-2 y:WIDTH-1 type:T)}
                    {BoxDestroyer destroy(x:HEIGHT-1 y:WIDTH-2 type:T)}
                    if NbrP >= 3 then
                        {BoxDestroyer destroy(x:2 y:WIDTH-1 type:T)}
                        {BoxDestroyer destroy(x:2 y:WIDTH-2 type:T)}
                        {BoxDestroyer destroy(x:3 y:WIDTH-1 type:T)}
                        if NbrP >= 4 then 
                            {BoxDestroyer destroy(x:HEIGHT-1 y:2 type:T)}
                            {BoxDestroyer destroy(x:HEIGHT-2 y:2 type:T)}
                            {BoxDestroyer destroy(x:HEIGHT-1 y:3 type:T)}
                        else skip end
                    else skip end
                else skip end
            else skip end
        
        else skip end
    end

    %-------------------------------------------------------------------------------------------------------
    % Level Generator
    %-------------------------------------------------------------------------------------------------------
    proc {LevelGen Msg}
        {BoxCreator setWall()}
        case Msg
        of dedale() then
            {BoxCreator setSequentialLines(type:'boxi')}
            {BoxDestroyer destroyTriangle(nbrplayers:NbrPlayers type:'boxi')}

        [] wood() then
            {BoxCreator setAllSpace()}
            {BoxDestroyer destroyTriangle(nbrplayers:NbrPlayers type:'boxd')}

        [] snow() then
            skip

        [] oz() then
            {BoxCreator set(x:5 y:11 type:boxd)}{BoxCreator set(x:5 y:12 type:boxd)}{BoxCreator set(x:5 y:13 type:boxd)}{BoxCreator set(x:5 y:14 type:boxd)}{BoxCreator set(x:5 y:15 type:boxd)}
            {BoxCreator set(x:5 y:19 type:boxd)}{BoxCreator set(x:5 y:20 type:boxd)}{BoxCreator set(x:5 y:21 type:boxd)}{BoxCreator set(x:5 y:22 type:boxd)}{BoxCreator set(x:5 y:23 type:boxd)}{BoxCreator set(x:5 y:24 type:boxd)}{BoxCreator set(x:5 y:25 type:boxd)}{BoxCreator set(x:5 y:26 type:boxd)}
            {BoxCreator set(x:6 y:10 type:boxd)}{BoxCreator set(x:6 y:11 type:boxd)}{BoxCreator set(x:6 y:15 type:boxd)}{BoxCreator set(x:6 y:16 type:boxd)}{BoxCreator set(x:6 y:26 type:boxd)}
            {BoxCreator set(x:7 y:9 type:boxd)}{BoxCreator set(x:7 y:10 type:boxd)}{BoxCreator set(x:7 y:16 type:boxd)}{BoxCreator set(x:7 y:17 type:boxd)}{BoxCreator set(x:7 y:25 type:boxd)}
            {BoxCreator set(x:8 y:9 type:boxd)}{BoxCreator set(x:8 y:17 type:boxd)}{BoxCreator set(x:8 y:24 type:boxd)}
            {BoxCreator set(x:9 y:9 type:boxd)}{BoxCreator set(x:9 y:17 type:boxd)}{BoxCreator set(x:9 y:23 type:boxd)}
            {BoxCreator set(x:10 y:9 type:boxd)}{BoxCreator set(x:10 y:17 type:boxd)}{BoxCreator set(x:10 y:23 type:boxd)}
            {BoxCreator set(x:11 y:9 type:boxd)}{BoxCreator set(x:11 y:17 type:boxd)}{BoxCreator set(x:11 y:22 type:boxd)}
            {BoxCreator set(x:12 y:9 type:boxd)}{BoxCreator set(x:12 y:17 type:boxd)}{BoxCreator set(x:12 y:22 type:boxd)}
            {BoxCreator set(x:13 y:9 type:boxd)}{BoxCreator set(x:13 y:10 type:boxd)}{BoxCreator set(x:13 y:16 type:boxd)}{BoxCreator set(x:13 y:17 type:boxd)}{BoxCreator set(x:13 y:21 type:boxd)}
            {BoxCreator set(x:14 y:10 type:boxd)}{BoxCreator set(x:14 y:11 type:boxd)}{BoxCreator set(x:14 y:15 type:boxd)}{BoxCreator set(x:14 y:16 type:boxd)}{BoxCreator set(x:14 y:20 type:boxd)}
            {BoxCreator set(x:15 y:19 type:boxd)}{BoxCreator set(x:15 y:20 type:boxd)}{BoxCreator set(x:15 y:21 type:boxd)}{BoxCreator set(x:15 y:22 type:boxd)}{BoxCreator set(x:15 y:23 type:boxd)}{BoxCreator set(x:15 y:24 type:boxd)}{BoxCreator set(x:15 y:25 type:boxd)}{BoxCreator set(x:15 y:26 type:boxd)}
            {BoxCreator set(x:15 y:11 type:boxd)}{BoxCreator set(x:15 y:12 type:boxd)}{BoxCreator set(x:15 y:13 type:boxd)}{BoxCreator set(x:15 y:14 type:boxd)}{BoxCreator set(x:15 y:15 type:boxd)}
        
        [] classical() then
            for I in 2..17 do
                if I mod 2 == 0 then 
                    {BoxCreator setLinePoint(axe:x index:I type:boxd)}
                else
                    skip
                end
            end
            for I in 2..35 do
                if I mod 2 == 0 then 
                    {BoxCreator setLinePoint(axe:y index:I type:boxd)}
                else
                    skip
                end
            end
            for I in 2..17 do
                if I mod 2 == 1 then 
                    {BoxCreator setLinePoint(axe:x index:I type:boxi)}
                else
                    skip
                end
            end
            {BoxDestroyer destroyTriangle(nbrplayers:NbrPlayers type:'boxd')}
            {BoxDestroyer destroyTriangle(nbrplayers:NbrPlayers type:'boxi')}
        
        [] random() then
            for _ in 1..30 do
                {BoxCreator set(x:{OS.rand} mod (17 + 1 - 2) + 2 y:{OS.rand} mod (34 + 1 - 2) + 2 type:boxi)}
            end 
            for _ in 1..100 do 
                {BoxCreator set(x:{OS.rand} mod (17 + 1 - 2) + 2 y:{OS.rand} mod (34 + 1 - 2) + 2 type:boxd)}
            end
            {BoxDestroyer destroyTriangle(nbrplayers:NbrPlayers type:'boxd')}
            {BoxDestroyer destroyTriangle(nbrplayers:NbrPlayers type:'boxi')}
        
        else skip end
    end

    %-------------------------------------------------------------------------------------------------------
    % Food Generator
    %-------------------------------------------------------------------------------------------------------
    proc {Food Nb} E1 E2 D 
        X = {OS.rand} mod ((HEIGHT-1)+1-2)+2
        Y = {OS.rand} mod ((WIDTH-1)+1-2)+2
        Index = {FindIndexe X Y}
        Timefix = Nb*1000
    in
        {Game emptyindex(index:Index empty:E1)} 
        if (E1 == true) then
            {Game add(index:Index value:'food')}
            {Gui food(X Y)}
            D = {OS.rand} mod (20000 + 1 - (12000+Timefix)) + (12000+Timefix)
            thread
                {Delay 10000}
                {Game remove(index:Index value:'food' where:map)}
                {Game emptyindex(index:Index empty:E2)}
                if (E2 == true) then
                    {Gui reset(X Y)} 
                else skip
                end
            end
            {Delay D}
            {Food Nb}
            
        else
            {Food Nb}
        end
    end

    %-------------------------------------------------------------------------------------------------------
    % Player Generator
    %-------------------------------------------------------------------------------------------------------
    proc {CreatePlayers MaxP ?P1 ?P2 ?P3 ?P4}
        if MaxP >= 1 then P1 = {NewActivePort Player init(x:2 y:2 id:a)}
            if MaxP >= 2 then P2 = {NewActivePort Player init(x:HEIGHT-1 y:WIDTH-1 id:b)}
                if MaxP >= 3 then P3 = {NewActivePort Player init(x:2 y:WIDTH-1 id:c)}
                    if MaxP >= 4 then P4 = {NewActivePort Player init(x:HEIGHT-1 y:2 id:d)}  
                    else skip end
                else skip end
            else skip end
        else {System.show error} end
    end

    %-------------------------------------------------------------------------------------------------------
    % Bonus Checker
    %-------------------------------------------------------------------------------------------------------
    proc {CheckBonus Bonus ?Skins}
        if Bonus == true then
            Skins={GUI.caracterSelection NbrPlayers PlayerNames bonus}
        else 
            Skins={GUI.caracterSelection NbrPlayers PlayerNames players}
        end
        {Wait Skins}
    end

    %-------------------------------------------------------------------------------------------------------
    % Runner
    %-------------------------------------------------------------------------------------------------------
    %%% Show launch informations on terminal #1
    {System.showInfo ''}
    {System.showInfo '%*********************** Starting BomberOz **********************'}
    {System.showInfo '%**'}
    %%% Show informations on terminal #2
    {System.showInfo '% -------------------- completed'}
    {System.showInfo ''}

    {Property.put 'print.width' 1000}
    {Property.put 'print.depth' 1000}
    % Default Values
    HEIGHT = 18 % X
    WIDTH  = 35 % Y
    Args = {
        Application.getArgs
        record(
            height(single char:&h type:int default:HEIGHT)
            width(single char:&w type:int default:WIDTH)
        )
    }

    % GUI - Init the Settings Window 
    NbrPlayers PlayerNames Bonus CanGoOpposite LevelN

    thread {Delay 510*1000} {Application.exit 0} end % TODO:Remove this line to fully play to the game
    
    {GUI.createPlayerWindow NbrPlayers PlayerNames Bonus CanGoOpposite LevelN}
    % Wait player choose
    {Wait NbrPlayers} {Wait Bonus} {Wait CanGoOpposite} {Wait LevelN}

    % GUI - Init the Character Window if bonus password is correct
    Skins = {CheckBonus Bonus}
    
    % GUI - Init the Game Window
    Gui = {GUI.create init(h:Args.height w:Args.width handler:Handler pnames:PlayerNames)}
    
    % Set skins
    {Gui setskins(Skins)}

    % Init the Game (Server)
    Game = {NewActivePort Server init(lo:1 hi:WIDTH*HEIGHT players:NbrPlayers)}

    % Generate a Level
    {LevelGen {String.toAtom LevelN}}

    % Init Players
    P1 P2 P3 P4
    {CreatePlayers NbrPlayers P1 P2 P3 P4}

    % Launch Food Creation
    for I in 1..NbrPlayers do
        {Delay {OS.rand} mod (4000 + 1 - 1000) + 1000}
        thread
            {Food NbrPlayers} 
        end
    end
end
